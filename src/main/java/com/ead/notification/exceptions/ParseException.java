package com.ead.notification.exceptions;

public class ParseException extends BusinessException {
	
	public ParseException(String message) {
		super(message);		
	}

	private static final long serialVersionUID = 1L;

}
