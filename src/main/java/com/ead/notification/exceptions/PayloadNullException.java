package com.ead.notification.exceptions;

public class PayloadNullException extends BusinessException {
	
	public PayloadNullException(String message) {
		super(message);		
	}

	private static final long serialVersionUID = 1L;

}
