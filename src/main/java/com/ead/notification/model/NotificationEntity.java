package com.ead.notification.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.ead.notification.model.enums.NotificationStatusEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_notification")
@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class NotificationEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@EqualsAndHashCode.Include
	@Column(name = "notification_id")
	private UUID notificationId;
	
	@Column(name = "user_id")
	private UUID userId;
	
	private String title;
	private String message;
	
	@CreationTimestamp
	@Column(name = "creation_date")
	private LocalDateTime creationDate;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "notification_status")
	private NotificationStatusEnum notificationStatus;
	
	

}
