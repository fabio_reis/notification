package com.ead.notification.model.enums;

public enum EventActionTypeEnum {
	
	CREATE,
	UPDATE,
	DELETE

}
