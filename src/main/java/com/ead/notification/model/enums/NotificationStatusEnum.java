package com.ead.notification.model.enums;

public enum NotificationStatusEnum {
	
	CREATED,
	READ;

}
