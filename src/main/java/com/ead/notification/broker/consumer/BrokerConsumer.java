package com.ead.notification.broker.consumer;

import static com.ead.notification.utils.ValidationUtils.isNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.ead.notification.broker.mapper.UserNotificationMapper;
import com.ead.notification.exceptions.PayloadNullException;
import com.ead.notification.service.NotificationService;


@Component
@RabbitListener(queues = {"${decoder.broker.queue.notificationCommandQueue}" })
public class BrokerConsumer {
	
	private Logger logger =  LogManager.getLogger(this.getClass());	
	
	@Autowired
	private UserNotificationMapper mapper;		

	@Autowired
	private NotificationService service;
	
	@RabbitHandler
	public void receive(@Payload String message) {
		
		if(isNull(message))
			throw new PayloadNullException("payload empty");
		
		service.save(mapper.toEntity(message));			
		
		logger.info("userNotificationt: " + "consumed: {}", message);
		
	}
	
}
