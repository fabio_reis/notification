package com.ead.notification.broker.mapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.notification.broker.dto.UserNotificationCommandDTO;
import com.ead.notification.exceptions.ParseException;
import com.ead.notification.model.NotificationEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class UserNotificationMapper {
	
	private Logger logger =  LogManager.getLogger(this.getClass());
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public NotificationEntity toEntity(String payload) {
		try {
			return modelMapper.map(objectMapper.readValue(payload.toString(), UserNotificationCommandDTO.class), NotificationEntity.class);			
		} catch (JsonProcessingException e) {
			logger.error("Error try to parse payload");
			throw new ParseException("Error try to parse payload");
		}
	}

}
