package com.ead.notification.broker.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqUserNotificationQueueConfig {
	
	@Value("${decoder.broker.exchange.notificationCommandExchange}")
	private String exchangeTopicNotification;
	
	@Value("${decoder.broker.queue.notificationCommandQueue}")
	private String userNotificationQueue;
	
	@Value("${decoder.broker.key.notificationCommandKey}")
	private String userNotificationKey;
	
	@Bean
	public TopicExchange notificationTopicExchange() {
		return new TopicExchange(exchangeTopicNotification);
	}
	
	@Bean
	public Queue userNotificationQueue() {
		return new org.springframework.amqp.core.Queue(userNotificationQueue);
	}
	
	@Bean
	public Binding bindUserNotificationQueue() {
		return BindingBuilder
				.bind(userNotificationQueue())
				.to(notificationTopicExchange())
				.with(userNotificationKey);
			
	}

}
