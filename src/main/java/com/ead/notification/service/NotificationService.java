package com.ead.notification.service;

import static com.ead.notification.utils.ValidationUtils.isNull;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ead.notification.exceptions.EntityRegistredException;
import com.ead.notification.model.NotificationEntity;
import com.ead.notification.model.enums.NotificationStatusEnum;
import com.ead.notification.repository.NotificationRepository;

@Service
public class NotificationService {
	
	@Autowired
	private NotificationRepository notificationRepository;
	
	@Transactional
	public void save(NotificationEntity notification) {
		validate(notification);
		
		notification.setNotificationStatus(NotificationStatusEnum.CREATED);
		notificationRepository.save(notification);
	}
	
	public List<NotificationEntity> findAllByUserId(UUID userId, Pageable pageable){		
		return notificationRepository.findAllByUserId(userId, pageable)
				.getContent();		
		
	}
	
	private void validate(NotificationEntity notification) {
		
		if(isNull(notification.getMessage()))
			throw new EntityRegistredException("message empty");
		
		if(isNull(notification.getTitle()))
			throw new EntityRegistredException("title empty");
		
		if(isNull(notification.getUserId()))
			throw new EntityRegistredException("userId empty");		
		
	}

}
