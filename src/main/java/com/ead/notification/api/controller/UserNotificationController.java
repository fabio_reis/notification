package com.ead.notification.api.controller;

import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ead.notification.api.dto.NotificationResponseDTO;
import com.ead.notification.api.mapper.NotificationMapper;
import com.ead.notification.service.NotificationService;

@RestController
@RequestMapping("v1/users/{userId}/notifications")
public class UserNotificationController {
	
	Logger logger =  LogManager.getLogger(this.getClass());
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private NotificationMapper notificationMapper;
	
	@GetMapping
	public ResponseEntity<Page<NotificationResponseDTO>> getAllNotificationsByUser(
			@PathVariable("userId") UUID userId,
			@PageableDefault(page = 0, size = 10, sort = "userId") Pageable pageable){
		
		logger.info("try do get notifications to user: {}", userId);
		
		 List<NotificationResponseDTO> notifications =
				 notificationMapper.toCollectionModelDTO(notificationService.findAllByUserId(userId, pageable));		
		
		   return ResponseEntity.ok(
				   new PageImpl<>(notifications, pageable, notifications.size())
		   );		
		
	}

}
