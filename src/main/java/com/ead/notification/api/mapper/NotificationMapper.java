package com.ead.notification.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.notification.api.dto.NotificationResponseDTO;
import com.ead.notification.model.NotificationEntity;

@Component
public class NotificationMapper {
	
	@Autowired
	private ModelMapper mapper;
	
	public NotificationResponseDTO toModelDTO(NotificationEntity notification) {			
		return mapper.map(notification, NotificationResponseDTO.class);	
	}	
	
	public List<NotificationResponseDTO> toCollectionModelDTO(List<NotificationEntity> users){
		return users.stream()
				.map(user -> toModelDTO(user))
				.collect(Collectors.toList());
	}	

}
