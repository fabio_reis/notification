CREATE TABLE TB_NOTIFICATION (
    notification_id UUID not null,
    user_id UUID not null,
    title VARCHAR(60) not null,
    message VARCHAR not null,
    creation_date TIMESTAMP NOT NULL,
    notification_status VARCHAR(30) NOT NULL, 	   	 
    primary key(notification_id)    	
);