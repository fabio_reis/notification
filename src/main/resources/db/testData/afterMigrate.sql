TRUNCATE TABLE notification.TB_NOTIFICATION CASCADE;

INSERT INTO notification.tb_notification(
	notification_id,
	user_id,
	title,
	message,
	creation_date,
	notification_status	
)
VALUES(
	
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	(SELECT user_id FROM auth_user.tb_users ORDER BY user_name ASC LIMIT 1),
	'title',
	'message1',
	'2022-11-02 00:00:00',
	'CREATED'	
);

INSERT INTO notification.tb_notification(
	notification_id,
	user_id,
	title,
	message,
	creation_date,
	notification_status	
)
VALUES(
	
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	(SELECT user_id FROM auth_user.tb_users ORDER BY user_name DESC LIMIT 1),
	'title',
	'message2',
	'2020-01-01 00:00:00',
	'READ'
);


